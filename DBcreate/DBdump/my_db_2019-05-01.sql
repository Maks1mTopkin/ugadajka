﻿# ************************************************************
# Sequel Pro SQL dump
# Version 5438
#
# https://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 8.0.0-dmr)
# Database: my_db
# Generation Time: 2019-05-01 08:34:58 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
SET NAMES utf8mb4;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table gameTexts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `gameTexts`;

CREATE TABLE `gameTexts` (
  `question` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `answer` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `gameTexts` WRITE;
/*!40000 ALTER TABLE `gameTexts` DISABLE KEYS */;

INSERT INTO `gameTexts` (`question`, `answer`)
VALUES
	('Зимой и летом одним цветом.','ель ёлка'),
	('Что это такое: Синий, большой, с усами и полностью набит зайцами.','тролейбус'),
	('Кто под проливным дождем не намочит волосы?','лысый'),
	('Какое растение всё знает?','хрен'),
	('Какое колесо не крутится при правом развороте?','запасное'),
	('Что такое: самое доброе в мире привидение с моторчиком?','Запорожец'),
	('С когтями, а не птица, летит и матерится.','Электромонтер');

/*!40000 ALTER TABLE `gameTexts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table labels
# ------------------------------------------------------------

DROP TABLE IF EXISTS `labels`;

CREATE TABLE `labels` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `txtLabels` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `texts` varchar(800) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `labels` WRITE;
/*!40000 ALTER TABLE `labels` DISABLE KEYS */;

INSERT INTO `labels` (`id`, `txtLabels`, `texts`)
VALUES
	(1,'message.right','Вы абсолютно правы !!'),
	(2,'message.wrong','Вы ввели неправильный ответ, подумайте ... '),
	(3,'message.end','Вопросы закончились, надеемся Вам было интересно.'),
	(4,'label.btn.answer','Ответить'),
	(5,'message.start','Давайте поиграем в игру - Угадайку.'),
	(6,'label.btn.start','Начать сначала !'),
	(7,'message.rules','В этой игре, Вам предстоит проверить свою сообразительность и в какой то мере эрудированность. Всё, что Вам нужно, для победы - это правильно ответить на все вопросы ! Если затрудняетесь с ответом, то можно воспользоваться подсказкой, которая подскажет Вам ответ. Для этого необходимо навести курсор мышки на надпись \'Показать ответ\'.'),
	(8,'message.gameName','Угадайка'),
	(9,'label.gameBtn','Начать игру !'),
	(10,'message.cheat','Показать ответ'),
	(11,'label.DBbutton','Показать результаты');

/*!40000 ALTER TABLE `labels` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `question` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `answer` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

INSERT INTO `user` (`question`, `answer`)
VALUES
	('Зимой и летом одним цветом.','ель'),
	('Зимой и летом одним цветом.','ель'),
	('Что это такое: Синий, большой, с усами и полностью набит зайцами.','тролейбус'),
	('Зимой и летом одним цветом.','dsa'),
	('Что это такое: Синий, большой, с усами и полностью набит зайцами.','das'),
	('Кто под проливным дождем не намочит волосы?','das'),
	('Какое растение всё знает?','dsa');

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
